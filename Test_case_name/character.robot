*** Settings ***
Resource    ../Test_case_name/myKeywords.resource
*** Test Cases ***
#There might be multiple matching error , but when your run your test it work , ide problem with no effect on tests 
Test "
    testing "
Test '
    testing '
Test ` echo test `
    testing ` echo test `
Test $test
    testing dollar test
#modif
Test &;| echo test
    testing &;| echo test
Test echo test > /tmp
    testing echo test > /tmp
Test ^
    testing ^
Test %test%
    testing %test%
Test ${echo test}
    testing \${echo test}    
Test backslash \\
    testing backslash
Test slash / 
    testing slash
Test $(echo test)
    testing $(echo test)
Test #test
    testing #test
Test 👩‍❤️‍💋‍👨 𨱏
    testing 👩‍❤️‍💋‍👨𨱏
Test [test]
    testing [test]
Test "test"
    testing "test"
Test 'test'
    testing 'test'
#modif
Test !?,+-_~@€çé=àû
    testing !?,+-_~@€çé=àû
Test *
    testing *
Test .
    testing .
